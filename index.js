const express = require('express');
const controllers = require('./app/controllers');
const PORT = process.env.PORT || 3000;

const app = express();

app.set('view engine', 'ejs');
app.use(express.json());

app.post('api/user/register', controllers.api.v1.userControl.register);

app.listen(PORT, ()=>{
    console.log(`listening at http://localhost:${PORT}`)
});