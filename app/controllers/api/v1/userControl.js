const userService = require('../../../service/userService');

module.exports = {
    async register(req, res){
        userService.register(req.body).then((user)=>{
            res.status(200).json({
                status : 'ok',
                user
            })
        }).catch((err)=>{
            res.status(400).json({
                status : 'fail',
                message : err.message
            })
        })
    }
}