// Request handler
module.exports = {
    onLost(req,res){
        res.status(404).json({
            status : "Fail",
            message : "Not found!"
        })
    },

    onError(req,res){
        res.status(401).json({
            status : "error",
            error : {
                name : err.name,
                message : err.message
            }
        })
    }
};