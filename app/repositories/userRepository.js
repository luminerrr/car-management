const { User } = require('../models');

module.exports = {
    create(createArgs){
        return User.create(createArgs)
    },

    findOne(email){
        return User.findOne(email)
    },

    findByPk(id){
        return User.findByPk(id)
    },

    delete(id){
        return User.destroy(id)
    }
}