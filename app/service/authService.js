const userRepository = require('../repositories/userRepository');

module.exports = {
    async encryptPassword(password){
        return new Promise((resolve, reject)=>{
            bcrypt.hash(password, SALT, (err, encryptedPassword)=>{
                if(!!err){
                    reject(err)
                    return
                }

                resolve(encryptedPassword)
            })
        })

    },

    async checkPassword(encryptedPassword, password){
        return new Promise((resolve, reject)=>{
            bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect)=>{
                if(!!err){
                    reject(err)
                    return
                }

                resolve(isPasswordCorrect)
            })
        })
    },
}