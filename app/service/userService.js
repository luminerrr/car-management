const userRepository = require('../repositories/userRepository');
const authService = require('./authService');
const bcrypt = require('bcryptjs');
const SALT = 10;

module.exports = {
    async register(requestBody){
        const email = requestBody.email;
        const password = requestBody.password;
        const encryptedPassword = await authService.encryptPassword(password);
        return userRepository.create({email, encryptedPassword});
    },

    delete(id){
        return userRepository.delete(id)
    },

    getId(id){
        return userRepository.findByPk(id)
    },

    getEmail(email){
        return userRepository.findOne(email)
    },

}